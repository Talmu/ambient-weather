import React from "react";
import "./App.css";
import { BrowserRouter } from "react-router-dom";
import Router from "./Router";
import { makeStyles } from "@material-ui/core/styles";
import Image from "./Images/background.jpg";

const App = () => {
  const classes = useStyles();

  return (
    <BrowserRouter>
      <div className={classes.background}>
        <div className={classes.root}>
          <Router />
        </div>
      </div>
    </BrowserRouter>
  );
};

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(4, 4),
  },
  background: {
    backgroundImage: `url(${Image})`,
    height: "100%",
    width: "100%",
    backgroundPosition: "center",
    backgroundSize: "cover",
    position: "absolute",
  },
}));

export default App;
