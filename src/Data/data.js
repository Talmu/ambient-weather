export const fetchData = async (API_URL, setData, setError) => {
  try {
    const response = await fetch(API_URL);
    if (response.ok) {
      const data = await response.json();
      setData(data);
    }
  } catch (error) {
    setError(true);
  }
};

export const getColumnsName = (rowObj) => Object.keys(rowObj);

export const getColumnsObj = (cols) =>
  cols.map((col) => ({
    field: col,
    headerName: col.charAt(0).toUpperCase() + col.slice(1),
    width: 160,
  }));

export const addIdField = (data) =>
  data.map((obj, index) => ({ id: index, ...obj }));

export const filterByName = (items, filterValue) => {
  const value = filterValue.toLowerCase();
  return items.filter((item) => item.info.name.toLowerCase().includes(value));
};
