import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { API_KEY, APP_KEY } from "./MainPage";
import { fetchData } from "../Data/data";
import { getColumnsName, getColumnsObj, addIdField } from "../Data/data";
import { Typography, Grid } from "@material-ui/core";
import { DataGrid } from "@mui/x-data-grid";
import { makeStyles } from "@material-ui/core/styles";
import PageError from "./PageError";
import Loading from "../components/Loading";
import BackButton from "../components/BackButton";

const DevicePage = () => {
  const { macAddress } = useParams();
  const classes = useStyles();
  const [details, setDetails] = useState(null);
  const [isLoading, setLoading] = useState(true);
  const [rows, setRows] = useState([]);
  const [columns, setColumns] = useState([]);
  const [error, setError] = useState(false);

  const API_URL = `https://rt.ambientweather.net/v1/devices/${macAddress}?applicationKey=${APP_KEY}&apiKey=${API_KEY}`;

  const handleDetailsChanged = (data) => setDetails(data);
  const handleError = (bool) => setError(bool);

  useEffect(() => {
    fetchData(API_URL, handleDetailsChanged, handleError);
  }, [API_URL]);

  useEffect(() => {
    const updateTable = () => {
      setColumns(getColumnsObj(getColumnsName(details[0])));
      setRows(addIdField(details));
    };

    if (details) {
      updateTable();
      setLoading(false);
    }
  }, [details]);

  return error ? (
    <PageError />
  ) : isLoading ? (
    <Loading isLoading={isLoading} />
  ) : (
    <Grid container spacing={3} direction="column">
      <Grid item className={classes.back}>
        <BackButton />
      </Grid>
      <Grid item>
        <Typography className={classes.title} variant="h4" gutterBottom>
          Device Data
        </Typography>
      </Grid>
      <Grid item className={classes.table}>
        <DataGrid rows={rows} columns={columns} />
      </Grid>
    </Grid>
  );
};

export default DevicePage;

const useStyles = makeStyles((theme) => ({
  title: {
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
  back: { position: "absolute", padding: theme.spacing(5, 5, 0, 0) },
  table: {
    height: 600,
    width: "100%",
  },
}));
