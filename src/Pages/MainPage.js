import React, { useState, useEffect } from "react";
import { List, Grid } from "@material-ui/core";
import Item from "../components/Item";
import SearchField from "../components/SearchField";
import { fetchData } from "../Data/data";
import PageError from "./PageError";
import Loading from "../components/Loading";

export const API_KEY = process.env.REACT_APP_API_KEY;
export const APP_KEY = process.env.REACT_APP_APPLICATION_KEY;

const MainPage = () => {
  const [isLoading, setLoading] = useState(true);
  const [devices, setDevices] = useState(null);
  const [items, setItems] = useState(null);
  const [error, setError] = useState(false);

  const API_URL = `https://rt.ambientweather.net/v1/devices?applicationKey=${APP_KEY}&apiKey=${API_KEY}`;

  const handleDataChanged = (data) => {
    setDevices(data);
    setItems(data);
  };
  const handleError = (bool) => setError(bool);
  const handleItemsChanged = (data) => setItems(data);

  useEffect(() => {
    setLoading(true);
    fetchData(API_URL, handleDataChanged, handleError);
    setLoading(false);
  }, [API_URL]);

  return error ? (
    <PageError />
  ) : isLoading ? (
    <Loading isLoading={isLoading} />
  ) : (
    <Grid container spacing={2} direction="column">
      <Grid item xs sm={6}>
        <SearchField data={devices} setItems={handleItemsChanged} />
      </Grid>
      <Grid item>
        <List>
          {items?.map((d, index) => (
            <Item key={index} info={d.info} macAddress={d.macAddress} />
          ))}
        </List>
      </Grid>
    </Grid>
  );
};

export default MainPage;
