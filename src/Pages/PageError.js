import React from "react";

const PageError = () => {
  return (
    <div id="wrapper">
      <img
        src="https://www.globalsign.com/application/files/9516/0389/3750/What_Is_an_SSL_Common_Name_Mismatch_Error_-_Blog_Image.jpg"
        alt="Error"
      />
      <div id="info">
        <h3>Oops! Something went wrong.</h3>
      </div>
    </div>
  );
};

export default PageError;
