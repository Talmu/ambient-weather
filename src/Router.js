import { Routes, Route } from "react-router-dom";
import MainPage from "./Pages/MainPage";
import DevicePage from "./Pages/DevicePage";

const Router = () => {
  return (
    <Routes>
      <Route path="/" element={<MainPage />} />
      <Route exact path="/device/:macAddress" element={<DevicePage />} />
    </Routes>
  );
};

export default Router;
