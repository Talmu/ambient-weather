import { IconButton } from "@material-ui/core";
import { useNavigate } from "react-router-dom";
import { ArrowBack } from "@material-ui/icons";

const BackButton = () => {
  const navigate = useNavigate();
  const handleBackClicked = () => navigate("/");

  return (
    <IconButton
      color="primary"
      style={{ position: "fixed" }}
      onClick={handleBackClicked}
    >
      <ArrowBack />
    </IconButton>
  );
};

export default BackButton;
