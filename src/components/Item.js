import React from "react";
import { useNavigate } from "react-router-dom";
import { ListItem, ListItemAvatar, ListItemText } from "@material-ui/core";
import { Avatar, Divider } from "@material-ui/core";
import { Smartphone } from "@material-ui/icons";

const Item = ({ info, macAddress }) => {
  const navigate = useNavigate();
  const handleItemClicked = () => navigate(`/device/${macAddress}`);

  return (
    <>
      <ListItem button onClick={handleItemClicked}>
        <ListItemAvatar>
          <Avatar>
            <Smartphone />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary={info.name} secondary={info.location} />
      </ListItem>
      <Divider />
    </>
  );
};

export default Item;
