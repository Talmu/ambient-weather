import React, { useState } from "react";
import { TextField, InputAdornment } from "@material-ui/core";
import { Search } from "@material-ui/icons";
import { filterByName } from "../Data/data";

const SearchField = ({ data, setItems }) => {
  const [value, setValue] = useState("");

  const handleChange = (e) => {
    const searchVal = e.target.value;
    setValue(searchVal);
    setItems(filterByName(data, searchVal));
  };

  return (
    <TextField
      variant="outlined"
      placeholder="Search by device name..."
      value={value}
      fullWidth
      onChange={handleChange}
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <Search color="primary" />
          </InputAdornment>
        ),
      }}
    />
  );
};

export default SearchField;
